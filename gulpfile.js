'use strict';
const {src, dest} = require('gulp');
const gulp = require('gulp');
const uglify = require('gulp-uglify-es').default;
const autoprefixer = require('gulp-autoprefixer');
const cssbeautify = require('gulp-cssbeautify');
const cssnano = require('gulp-cssnano');
const imagemin = require('gulp-imagemin');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const rigger = require('gulp-rigger');
const sass = require('gulp-sass');
const stripComments = require('gulp-strip-css-comments');
const del = require('del');
const fileinclude = require('gulp-file-include');
const ttf2woff = require('gulp-ttf2woff');
const ttf2woff2 = require('gulp-ttf2woff2');
const browsersync = require('browser-sync').create();
let fs = require('fs');
const cleanCSS = require('gulp-clean-css');

let path = {
    build: {
        html: 'dist/',
        js: 'dist/assets/js/',
        jsLibs: 'dist/assets/libs/',
        css: 'dist/assets/styles/',
        fonts: 'dist/assets/fonts/',
        images: 'dist/assets/img/',
        // video: 'dist/assets/video/',
    },
    src: {
        html: 'src/html/**/*.html',
        js: 'src/assets/js/**/*.js',
        jsLibs: 'src/assets/libs/**/*',
        scss: ['src/assets/styles/*.scss','src/assets/styles/components/**/*.scss','src/assets/styles/modules/**/*.scss','src/assets/styles/pages/**/*.scss',  'src/assets/styles/blocks/**/*.scss', 'src/assets/styles/pages/**/*.scss'],
        css: 'src/assets/css/**/*.css',
        fonts: 'src/assets/fonts/**',
        images: 'src/assets/img/**/*.{jpg,jpeg,png,svg,gif,ico,webp}',
        // video: 'src/assets/video/**/*.{mp4,webm}',
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/assets/js/**/*.js',
        jsLibs: 'src/assets/libs/**/*',
        scss: 'src/assets/styles/**/*.scss',
        css: 'src/assets/css/**/*.css',
        fonts: 'src/assets/fonts/',
        images: 'src/assets/img/**/*.{jpg,jpeg,png,svg,gif,ico,webp}',
        // video: 'src/assets/video/**/*{mp4,webm}',
    },
    clean: './dist/',
};

/* Tasks */
function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: './dist',
        },
        port: 3000,
        notify: false,
    });
}

function browserSyncReload(done) {
    browsersync.reload();
}

function html() {
    return src(path.src.html, {base: 'src/'})
        .pipe(plumber())
        .pipe(
            fileinclude({
                prefix: '%%',
                basepath: '@file',
            })
        )
        .pipe(rename({dirname: ''}))
        .pipe(dest(path.build.html))
        .pipe(browsersync.stream());
}

function scssToCss() {
    return src(path.src.scss, {base: 'src/assets/styles'})
        .pipe(plumber())
        .pipe(sass())
        .pipe(
            autoprefixer({
                browserlist: ['deafaults'],
                cascade: 'true',
            })
        )
        .pipe(cssbeautify())
        .pipe(dest(path.build.css))
        .pipe(
            cssnano({
                discardUnused: false,
                zindex: false,
                discardComments: {
                    removeAll: true,
                },
            })
        )
        .pipe(stripComments())
        .pipe(
            rename({
                suffix: '.min',
                extname: '.css',
            })
        )
        .pipe(dest(path.build.css))
        .pipe(browsersync.stream());
}

function addCss() {
    return src(path.src.css, {base: 'src/assets/css'}).pipe(plumber()).pipe(dest(path.build.css)).pipe(browsersync.stream());
}

function cleanCss() {
    return src(path.src.css).pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist'));
}

function js() {
    return src(path.src.js, {base: 'src/assets/js'})
        .pipe(plumber())
        .pipe(rigger())
        .pipe(gulp.dest(path.build.js))
        .pipe(uglify())
        .pipe(
            rename({
                suffix: '.min',
                extname: '.js',
            })
        )
        .pipe(dest(path.build.js))
        .pipe(browsersync.stream());
}

function jsLibs() {
    return src(path.src.jsLibs, {base: 'src/assets/libs'}).pipe(plumber()).pipe(dest(path.build.jsLibs));
}

function images() {
    return src(path.src.images)
        .pipe(
            imagemin([
                imagemin.gifsicle({interlaced: true}),
                imagemin.mozjpeg({quality: 75, progressive: true}),
                imagemin.optipng({optimizationLevel: 2}),
                imagemin.svgo({
                    plugins: [{removeViewBox: true}, {cleanupIDs: false}],
                }),
            ])
        )
        .pipe(dest(path.build.images));
}


function fonts() {
    src(path.src.fonts).pipe(dest(path.build.fonts));
    return src(path.src.fonts).pipe(dest(path.build.fonts));
}

function clean() {
    return del([path.clean]);
}

function cb() {
    return;
}

function watchFiles() {
    gulp.watch([path.watch.html], html);
    gulp.watch([path.watch.scss], scssToCss);
    gulp.watch([path.watch.css], addCss);
    gulp.watch([path.watch.js], js);
    gulp.watch([path.watch.jsLibs], jsLibs);
    gulp.watch([path.watch.fonts], fonts);
    gulp.watch([path.watch.images], images);

}

const build = gulp.series(clean, fonts, addCss, scssToCss, cleanCss, html, gulp.parallel(js, jsLibs, images), browserSync);

const watch = gulp.parallel(build, watchFiles);

/* Export Tasks */
exports.html = html;
exports.scssToCss = scssToCss;
exports.addCss = addCss;
exports.js = js;
exports.jsLibs = jsLibs;
exports.fonts = fonts;
exports.images = images;
// exports.video = video;
exports.clean = clean;
exports.build = build;
exports.watch = watch;
exports.default = watch;
